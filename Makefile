export INSTALL ?= install
export PREFIX  ?= /usr/local

all: .DEFAULT
.DEFAULT:
	$(MAKE) -C src $(MAKECMDGOALS)

indent:
	find -name \*.[ch] | grep -v 'list.h' | xargs indent -linux -l100 -psl -cs
